import React, { Component } from 'react';
import { Table,ButtonGroup,Button,Glyphicon ,Modal } from 'react-bootstrap';


class MaintainPromotionComponent extends Component {
    constructor(props, context) {
        super(props, context);
        var pro = this.loadAndSetPromotionFromStorage();
        this.handleEditClick = this.handleEditClick.bind(this);
        this.state = {
            all_promotion : pro,
            modal_show : false,
            modal_mode : "",
        };
        
    }
    loadAndSetPromotionFromStorage = () => {
        let _ps  = localStorage.getItem('res_promotions');
        let p_arr = JSON.parse(_ps);
        let _output =  [];
        for(var i = 0 ; i < p_arr.length ; i++){
            _output.push(<tr key={p_arr[i]._id}>
            <th>{i+1}</th>
            <th>{(p_arr[i].IS_CODE)?'CODE':'NO CODE'}</th>
            <th>{(p_arr[i].LESS_AMOUNT)}</th>
            <th>{(p_arr[i].CODE)}</th>
            <th>{(p_arr[i].DISCOUNT)}</th>
            <th>{(p_arr[i].CAL_METHOD == "*")?'PERCENT':'CASH'}</th>
            <th>{(p_arr[i].LIMIT_PAX)}</th>
            <th><Button bsStyle="info" pid={p_arr[i]._id} onClick={this.handleEditClick.bind(this)}><Glyphicon glyph="edit" /></Button><Button bsStyle="danger" pid={p_arr[i]._id} onClick={this.handleDeleteClick.bind(this)}><Glyphicon glyph="remove" /></Button></th>
            </tr>);
        }
        return _output;
    }
    handleEditClick = (e) =>{
       var p_id = e.currentTarget.getAttribute("pid");
       console.log(p_id);
       this.setState({
        all_promotion : this.state.all_promotion,
        modal_show : true,
        modal_mode : "EDIT",
    });
    }
    handleDeleteClick = (e) =>{
        var p_id = e.currentTarget.getAttribute("pid");
        console.log(p_id);
        this.setState({
            all_promotion : this.state.all_promotion,
            modal_show : true,
            modal_mode : "DELETE",
        });
    }
    handleAddClick = (e) =>{
        var p_id = e.currentTarget.getAttribute("pid");
        console.log(p_id);
        this.setState({
            all_promotion : this.state.all_promotion,
            modal_show : true,
            modal_mode : "ADD",
        });
    }
    handleModalConfirm = () =>{
        this.setState({
            all_promotion : this.state.all_promotion,
            modal_show : false,
            modal_mode : "",
        });
    }
    handleModalClose = () =>{
        this.setState({
            all_promotion : this.state.all_promotion,
            modal_show : false,
            modal_mode : "",
        });
    }

     createModal = () =>(
        <Modal show={this.state.modal_show} onHide={this.handleModalClose.bind(this)}>
        <Modal.Header closeButton>
          <Modal.Title>Modal heading</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          
        </Modal.Body>
        <Modal.Footer>
          <Button onClick={this.handleModalConfirm.bind(this)}>Confirm</Button>
          <Button onClick={this.handleModalClose.bind(this)}>Close</Button>
        </Modal.Footer>
      </Modal>
     )

    render() {
        return (
            <div>
                {this.createModal()}
                <ButtonGroup>
                    <Button bsStyle="success" onClick={this.handleAddClick.bind(this)}><Glyphicon glyph="plus" /></Button>
                </ButtonGroup>
                <Table striped bordered condensed hover responsive>
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Code Type</th>
                            <th>Less Amount</th>
                            <th>Code</th>
                            <th>Discount</th>
                            <th>Cal Method</th>
                            <th>Limit Pax</th>
                            <th>Edit/Delete</th>
                        </tr>
                    </thead>
                    <tbody>
                        {this.state.all_promotion}
                    </tbody>
                </Table>
            </div>
        );
    }
}

export default MaintainPromotionComponent;
