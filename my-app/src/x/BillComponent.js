import React, { Component } from 'react';

import { HelpBlock,ControlLabel, FormControl, FormGroup, Button, Nav, Navbar, NavItem } from 'react-bootstrap';




const BUFFET_PRICE = 459;

class BillCalculatorComponent extends Component {
    constructor(props, context) {
        super(props, context);
        this.handlePaxChange = this.handlePaxChange.bind(this);
        this.handlePromoChange = this.handlePromoChange.bind(this);

        this.state = {
            PAX: 0,
            PROMO_CODE: "",
            TOTAL_PRICE : 0,
            DISCOUNT : [],
            TOTAL_PRICE_DIS : 0,
            DISCOUNT_DESCRIPTION : ""
        };
    }
    handlePaxChange = (ex) => {

        
        var x = this.calculateDiscount(ex.target.value,this.state.PROMO_CODE);       

        this.setState(
            {
                PAX: ex.target.value,
                PROMO_CODE: this.state.PROMO_CODE,
                TOTAL_PRICE :x.TOTAL_PRICE,
                DISCOUNT :x.ALL_DISCOUNT,
                TOTAL_PRICE_DIS :x.PRICE_AFTER_DIS,
                DISCOUNT_DESCRIPTION :x.DESCRIPTION,
            }
        );

    }
    handlePromoChange = (ex) => {
        
        var x = this.calculateDiscount(this.state.PAX,ex.target.value);
        this.setState(
            {
                PAX: this.state.PAX,
                PROMO_CODE: ex.target.value,
                TOTAL_PRICE :x.TOTAL_PRICE,
                DISCOUNT :x.ALL_DISCOUNT,
                TOTAL_PRICE_DIS :x.PRICE_AFTER_DIS,
                DISCOUNT_DESCRIPTION :x.DESCRIPTION,
            }
        );

    }
    calculateDiscount = (pax,promo_code) =>{
        let all_promotions = localStorage.getItem('res_promotions');
        let p_arr = JSON.parse(all_promotions);
        let _all_discount = [];
        let total_price_bd = pax * BUFFET_PRICE;
        let _price_dis = total_price_bd;
        let description = "";
        for(let i =  0 ; i < p_arr.length ; i++){
            var check_code = false;
            var check_less_amount = false;
            var check_limit_pax = false;
            if(!p_arr[i].IS_CODE){
                check_code = true;
            }else if(p_arr[i].CODE == promo_code){
                check_code = true;
            }
            if(p_arr[i].LESS_AMOUNT == null){
                check_less_amount = true;
            }else if(parseInt(p_arr[i].LESS_AMOUNT) <= total_price_bd){
                check_less_amount = true;
            }
            if(p_arr[i].LIMIT_PAX == null){
                check_limit_pax = true;
            }else if(parseInt(p_arr[i].LIMIT_PAX) <= pax){
                check_limit_pax = true;
            }
            if(check_code && check_less_amount && check_limit_pax){
                if(p_arr[i].CAL_METHOD == "*"){
                    _all_discount.push(total_price_bd * p_arr[i].DISCOUNT);
                    _price_dis -= total_price_bd * p_arr[i].DISCOUNT;
                    
                description += (p_arr[i].DESCRIPTION) + "(-"+ (total_price_bd * p_arr[i].DISCOUNT) + ")" + " / ";
                }else{
                    _all_discount.push(p_arr[i].DISCOUNT);
                    _price_dis -= p_arr[i].DISCOUNT;
                    description += (p_arr[i].DESCRIPTION) + "(-"+ p_arr[i].DISCOUNT + ")" + " / ";
                }
                
            }
            
        }
        return {
            ALL_DISCOUNT : _all_discount,
            PRICE_AFTER_DIS : _price_dis,
            TOTAL_PRICE : total_price_bd,
            DESCRIPTION : description
        }

    }
    render() {
        return (
            <div>
                <h2>Bill Calculator</h2>
                <form>
                    <FormGroup
                        controlId="formBasicText"
                    >
                        <ControlLabel>PAX : </ControlLabel>
                        <FormControl
                            type="number"
                            value={this.state.PAX}
                            placeholder="Enter number of Pax"
                            onChange={this.handlePaxChange}
                        />
                        <ControlLabel>PROMO CODE : </ControlLabel>
                        <FormControl
                            type="text"
                            value={this.state.PROMO_CODE}
                            placeholder="Enter Promotion Code"
                            onChange={this.handlePromoChange}
                        />
                        <ControlLabel>TOTAL PRICE  : </ControlLabel>
                        <FormControl
                            type="number"
                            value={this.state.TOTAL_PRICE}
                        />
                        <ControlLabel>PRICE AFTER DISCOUNT  : </ControlLabel>
                        <FormControl
                            type="number"
                            value={this.state.TOTAL_PRICE_DIS}
                        />
                        <HelpBlock>{this.state.DISCOUNT_DESCRIPTION}</HelpBlock>

                    </FormGroup>
                </form>
                
            </div>
        );
    }
}
export default BillCalculatorComponent;
