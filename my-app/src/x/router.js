import React from 'react'
import {
    BrowserRouter as Router,
    Route,
    Link
} from 'react-router-dom'
import { Button, Nav, Navbar, NavItem } from 'react-bootstrap';
import BillCalculatorComponent from './BillComponent';
import MaintainPromotionComponent from './MaintainPComponent';
import { LinkContainer } from 'react-router-bootstrap';

const RouterTest = () => (
    <div>
        <Nav bsStyle="tabs">
            <LinkContainer to="/">
                <NavItem title="Item"> Bill Calculator
            </NavItem>
            </LinkContainer>
            <LinkContainer to="/promotion">
                <NavItem title="Item"> Maintain Promotion
            </NavItem>
            </LinkContainer>
            <LinkContainer to="/reservation">
                <NavItem title="Item"> Reservation
            </NavItem>
            </LinkContainer>
        </Nav>

        <Route exact path="/" component={BillCalculatorComponent} > </Route>
        <Route path="/promotion" component={MaintainPromotionComponent} />
        <Route path="/reservation" component={MaintainPromotionComponent} />
    </div>

)
export default RouterTest