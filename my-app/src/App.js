import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import RouterTest from './x/router';
import {
  BrowserRouter as Router,
  Route,
  Link
} from 'react-router-dom'

const fcl = () =>{
  let _ps  = localStorage.getItem('res_promotions');
  if(!_ps){
    _ps = [{
      _id : 1,
      IS_CODE : true,
      LESS_AMOUNT : 1000,
      CODE : "LUCKY ONE",
      DISCOUNT : 0.15,
      CAL_METHOD : "*",
      LIMIT_PAX : null,
      DESCRIPTION : "Discount 15% for coupon code 'LUCKY ONE' and bill is more than 1000 thb",
    },
    {
      _id : 2,
      IS_CODE : false,
      LESS_AMOUNT : null,
      CODE : null,
      DISCOUNT : 459,
      CAL_METHOD : "-",
      LIMIT_PAX : 4,
      DESCRIPTION :"4PAY3",
    },
    {
      _id : 3,
      IS_CODE : true,
      LESS_AMOUNT : null,
      CODE : "LUCKY TWO",
      DISCOUNT : 0.2,
      CAL_METHOD : "*",
      LIMIT_PAX : 2,
      DESCRIPTION :"Discount 20% for 2 customer when they present a coupon code as 'LUCKY TWO'",
    },
    {
      _id : 4,
      IS_CODE : false,
      LESS_AMOUNT : 6000,
      CODE : null,
      DISCOUNT : 0.25,
      CAL_METHOD : "*",
      LIMIT_PAX : null,
      DESCRIPTION :"Discount 25% when the bill is over 6000 Bath",
    }
  ];
  var myJSON = JSON.stringify(_ps);
  localStorage.setItem('res_promotions',myJSON);
  }
}


class App extends Component {
  constructor(props) {
    super(props);
    fcl();
    this.state = {
    };
  }
  render() {
    return (
     
      <Router>
         <RouterTest>
        
        </RouterTest>
        </Router>
    
    );
  }
}

export default App;
